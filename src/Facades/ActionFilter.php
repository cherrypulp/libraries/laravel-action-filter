<?php

namespace Cherrypulp\ActionFilter\Facades;

use Illuminate\Support\Facades\Facade;

class ActionFilter extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'action-filter';
    }
}
