# Laravel Action Filter

[![Build Status](https://travis-ci.org/cherrypulp/laravel-action-filter.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-action-filter)
[![Packagist](https://img.shields.io/packagist/v/cherrypulp/laravel-action-filter.svg)](https://packagist.org/packages/cherrypulp/laravel-action-filter)
[![Packagist](https://poser.pugx.org/cherrypulp/laravel-action-filter/d/total.svg)](https://packagist.org/packages/cherrypulp/laravel-action-filter)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-action-filter.svg)](https://packagist.org/packages/cherrypulp/laravel-action-filter)

Helper to create Action Filter like in Wordpress but in your Laravel/PHP project. Forked from : https://github.com/tormjens/Eventy.

## Installation

Install via composer

```bash
composer require cherrypulp/laravel-action-filter
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\ActionFilter\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Cherrypulp\ActionFilter\Facades\ActionFilter::class,
```

## Usage

### Actions

Anywhere in your code you can create a new action like so:

```php
use Cherrypulp\ActionFilter\Facades\Events as ActionFilter;

ActionFilter::action('my.hook', 'awesome');
```

The first parameter is the name of the hook; you will use this at a later point when you'll be listening to your hook. All subsequent parameters are sent to the action as parameters. These can be anything you'd like. For example you might want to tell the listeners that this is attached to a certain model. Then you would pass this as one of the arguments.

To listen to your hooks, you attach listeners. These are best added to your `AppServiceProvider` `boot()` method. 

For example if you wanted to hook in to the above hook, you could do:

```
ActionFilter::addAction('my.hook', function($what) {
    echo 'You are '. $what;
}, 20, 1);
```

Again the first argument must be the name of the hook. The second would be a callback. This could be a Closure, a string referring to a class in the application container (`MyNamespace\Http\Listener@myHookListener`), an array callback (`[$object, 'method']`) or a globally registered function `function_name`. The third argument is the priority of the hook. The lower the number, the earlier the execution. The fourth parameter specifies the number of arguments your listener accepts.

### Filters

Filters work in much the same way as actions and have the exact same build-up as actions. The most significant difference is that filters always return their value. 

To add a filter:

```php 
$value = ActionFilter::filter('my.hook', 'awesome');
```

If no listeners are attached to this hook, the filter would simply return `'awesome'`. 

This is how you add a listener to this filter (still in the `AppServiceProvider`):

```php
ActionFilter::addFilter('my.hook', function($what) {
    $what = 'not '. $what;
    return $what;
}, 20, 1);
```

The filter would now return `'not awesome'`. Neat!

You could use this in conjunction with the previous hook:

```php
ActionFilter::addAction('my.hook', function($what) {
    $what = ActionFilter::filter('my.hook', 'awesome');
    echo 'You are '. $what;
});
```

### Using in Blade

Given you have added the `EventBladeServiceProvider` to your config, there are two directives available so you can use this in your Blade templates.

Adding the same action as the one in the action example above:

```
@action('my.hook', 'awesome')
```

Adding the same filter as the one in the filter example above:

```
You are @filter('my.hook', 'awesome')

```

## Security

If you discover any security related issues, please email 
instead of using the issue tracker.

## Credits

- [Tor Morten Jensen](https://github.com/tormjens)
- [Daniel Sum](https://github.com/danielsum)
- [All contributors](https://github.com/cherrypulp/laravel-action-filter/graphs/contributors)

This package is bootstrapped with the help of
[cherrypulp/laravel-package-generator](https://github.com/cherrypulp/laravel-package-generator).
