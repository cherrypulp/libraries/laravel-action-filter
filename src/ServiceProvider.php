<?php

namespace Cherrypulp\ActionFilter;

use Cherrypulp\ActionFilter\Facades\ActionFilter;
use Illuminate\Support\Facades\Blade;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        /*
         * Adds a directive in Blade for actions
         */
        Blade::directive('action', function ($expression) {
            return "<?php ActionFilter::action({$expression}); ?>";
        });

        /*
         * Adds a directive in Blade for filters
         */
        Blade::directive('filter', function ($expression) {
            return "<?php echo ActionFilter::filter({$expression}); ?>";
        });
    }

    public function register()
    {
        $this->app->bind('action-filter', function () {
            return new Events();
        });
    }
}
